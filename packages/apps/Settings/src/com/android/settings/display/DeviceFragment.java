package com.android.settings.display;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.UiModeManager;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.SearchIndexableResource;
import android.provider.Settings;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.PreferenceScreen;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView.FindListener;

import android.util.Log;
import android.view.Display.Mode;
import android.view.View;
import android.widget.TextView;
import android.os.DisplayOutputManager;
import android.hardware.display.DisplayManager;
import android.os.SystemProperties;
import android.support.annotation.Keep;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.data.ConstData;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.logging.MetricsProto.MetricsEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;

import android.support.v14.preference.SwitchPreference;

public class DeviceFragment extends SettingsPreferenceFragment implements
Preference.OnPreferenceChangeListener{

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.DISPLAY;
    }
    protected static final String TAG = "DeviceFragment";
    public static final String KEY_RESOLUTION = "resolution";
    public static final String KEY_COLOR = "color";
    public static final String KEY_ZOOM = "zoom";
    public static final String KEY_ADVANCED_SETTINGS = "advanced_settings";
    protected PreferenceScreen mPreferenceScreen;
    /**
     * 分辨率设置
     */
    protected ListPreference mResolutionPreference;
    /**
     * 屏幕颜色率设置
     */
    protected ListPreference mColorPreference;
    /**
     * 缩放设置
     */
    protected Preference mZoomPreference;
    /**
     * 高级设置
     */
    protected Preference mAdvancedSettingsPreference;
    /**
     * 当前显示设备对应的信息
     */
    protected DisplayInfo mDisplayInfo;

    /**
     * 标识平台
     */
    protected String mStrPlatform;

    protected boolean mIsUseDisplayd;
    /**
     * 显示管理
     */
    protected DisplayManager mDisplayManager;
    /**
    * 原来的分辨率
    */
    private String mOldResolution;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity activity = getActivity();

        addPreferencesFromResource(R.xml.display_device);

        initData();
        initView();
        initEvent();
    }
    protected void initData(){
        mStrPlatform = SystemProperties.get("ro.board.platform");
        mIsUseDisplayd = SystemProperties.getBoolean("ro.rk.displayd.enable", true);
        mDisplayManager = (DisplayManager)getActivity().getSystemService(Context.DISPLAY_SERVICE);
        mPreferenceScreen = getPreferenceScreen();
	mColorPreference = (ListPreference) findPreference(KEY_COLOR);
        if (!mIsUseDisplayd) {
            mDisplayInfo = getDisplayInfo();
        } else {
            Intent intent = getActivity().getIntent();
            mDisplayInfo = (DisplayInfo) intent.getExtras().getSerializable(ConstData.IntentKey.DISPLAY_INFO);
        }
        
        mAdvancedSettingsPreference = findPreference(KEY_ADVANCED_SETTINGS);
        mResolutionPreference = (ListPreference) findPreference(KEY_RESOLUTION);
        mColorPreference = (ListPreference) findPreference(KEY_COLOR);

        mZoomPreference = findPreference(KEY_ZOOM);
        
        
        
        if(mStrPlatform.contains("3328"))
            mPreferenceScreen.removePreference(mColorPreference);
    }
    
    

    protected void initView(){
        if (mDisplayInfo == null)
            return;
        mResolutionPreference.setEntries(mDisplayInfo.getModes());
        mResolutionPreference.setEntryValues(mDisplayInfo.getModes());
        mColorPreference.setEntries(mDisplayInfo.getColors());
        mColorPreference.setEntryValues(mDisplayInfo.getColors());
    }


    protected void initEvent(){
          mResolutionPreference.setOnPreferenceChangeListener(this);
        //mZoomPreference.setOnPreferenceClickListener(this);
	 mColorPreference.setOnPreferenceChangeListener(this);
    }
    

    @Override
    public void onResume() {
        super.onResume();
        updateResolutionValue();
    }
    
    /**
     * 还原分辨率值
     */
    public void updateResolutionValue(){
        String resolutionValue = null;
        if(!mIsUseDisplayd){
            resolutionValue = DrmDisplaySetting.getCurDisplayMode(mDisplayInfo);
            Log.i(TAG, "3399 resolutionValue:" + resolutionValue);
            if(resolutionValue != null)
                mResolutionPreference.setValue(resolutionValue);
        }else{
            DisplayOutputManager displayOutputManager = null;
            try{
                displayOutputManager = new DisplayOutputManager();
                resolutionValue = displayOutputManager.getCurrentMode(mDisplayInfo.getDisplayId() == 0 ? 0 : 1, mDisplayInfo.getType());
            }catch (Exception e){
                Log.i(TAG, "updateResolutionValue->exception:" + e);
            }
            if(resolutionValue != null)
                mResolutionPreference.setValue(resolutionValue);
            if(mOldResolution == null)
                mOldResolution = resolutionValue;
        }
    }
    
    @Override
    public boolean onPreferenceChange(Preference preference, Object obj) {
        Log.i(TAG, "onPreferenceChange:" + obj);
        if(preference == mResolutionPreference){
            if(!mIsUseDisplayd){
                int index = mResolutionPreference.findIndexOfValue((String)obj);
                DrmDisplaySetting.setDisplayModeTemp(mDisplayInfo, index);
                showConfirmSetModeDialog();
            }else{
                DisplayOutputManager displayOutputManager = null;
                try{
                    displayOutputManager = new DisplayOutputManager();
                }catch (Exception e){
                    Log.i(TAG, "onPreferenceChange->exception:" + e);
                }

                if(displayOutputManager != null){
                    displayOutputManager.setMode(mDisplayInfo.getDisplayId(), mDisplayInfo.getType(), (String)obj);
                    showConfirmSetModeDialog();
                }
            }

        }else if (preference == mColorPreference) {
            if (!mIsUseDisplayd) {
                DrmDisplaySetting.setColorMode(mDisplayInfo.getDisplayId(), mDisplayInfo.getType(), (String) obj);
            } else {
            }
        }
        return true;
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if(preference == mZoomPreference) {
            Intent screenScaleIntent = new Intent(getActivity(), ScreenScaleActivity.class);
            screenScaleIntent.putExtra(ConstData.IntentKey.PLATFORM, mStrPlatform);
            screenScaleIntent.putExtra(ConstData.IntentKey.DISPLAY_INFO, mDisplayInfo);
            getActivity().startActivity(screenScaleIntent);
        } else if (preference == mResolutionPreference) {
            //updateResolutionValue();
        }else if (preference == mAdvancedSettingsPreference) {
            Intent advancedIntent = new Intent(getActivity(), AdvancedDisplaySettingsActivity.class);
            advancedIntent.putExtra(ConstData.IntentKey.DISPLAY_ID, mDisplayInfo.getDisplayId());
            getActivity().startActivity(advancedIntent);
        }
        return true;
    }
    
    
    @SuppressLint("NewApi")
    protected void showConfirmSetModeDialog() {
        DialogFragment df = ConfirmSetModeDialogFragment.newInstance(mDisplayInfo, new ConfirmSetModeDialogFragment.OnDialogDismissListener() {
            @Override
            public void onDismiss(boolean isok) {
                Log.i(TAG, "showConfirmSetModeDialog->onDismiss->isok:" + isok);
                Log.i(TAG, "showConfirmSetModeDialog->onDismiss->mOldResolution:" + mOldResolution);
                if(!mIsUseDisplayd)
                    updateResolutionValue();
                else{
                    DisplayOutputManager displayOutputManager = null;
                    try{
                        displayOutputManager = new DisplayOutputManager();
                    }catch (Exception e){
                        Log.i(TAG, "onPreferenceChange->exception:" + e);
                    }
                    if(isok && displayOutputManager != null){
                        displayOutputManager.saveConfig();
                    }else if(!isok && displayOutputManager != null && mOldResolution != null){
                        //还原原来的分辨率
                        displayOutputManager.setMode(mDisplayInfo.getDisplayId(), mDisplayInfo.getType(), mOldResolution);
                    }
                    updateResolutionValue();
                }
            }
        });
        df.show(getFragmentManager(), "ConfirmDialog");
    }

    protected DisplayInfo getDisplayInfo() {
        return null;
    }

    
}
